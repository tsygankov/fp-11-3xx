module HW1
       ( hw1_1
       , hw1_2
       , fact2
       , isPrime
       , primeSum
       ) where

-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты

-- |Вычислить сумму двух аргументов
hw1_1 :: Integer -> Integer -> Integer
-- hw1_1 a b = error "hw1_1 is not implemented"
hw1_1 a b = a + b

-- |Вычислить сумму N членов ряда
--
--
--  N
-- ---
-- \    1
--  >  ---
-- /   k^k
-- ---
-- k=1
--
-- Использовать fromIntegral для перевода из Integer в Double
hw1_2 :: Integer -> Double
hw1_2 1 = 1.0
hw1_2 n = 1.0/fromIntegral(n^n) + hw1_2(n-1)

-- |Вычислить двойной факториал
-- n!! = 1*3*5*...*n, если n - нечетное
-- n!! = 2*4*6*...*n, если n - четное
fact2 :: Integer -> Integer
fact2 0 = 1
fact2 1 = 1
fact2 2 = 2
fact2 n = n * fact2(n-2)

-- |Проверить заданное число на простоту
-- Использовать div для целочисленного деления
-- или mod для остатка от деления
isModable :: Integer -> Integer -> Integer -> Bool
isModable n k last | k == last = n `mod` k == 0
                   | k > last = False
                   | k == 2 = n `mod` k == 0 || isModable n (k+1) last
                   | otherwise = n `mod` k == 0 || isModable n (k+2) last

isPrime :: Integer -> Bool
isPrime 1 = False
isPrime p = not (isModable p 2 (floor (sqrt (fromIntegral p))))

-- |Найти сумму всех простых чисел в диапазоне [a;b]
primeSum :: Integer -> Integer -> Integer
primeSum a b | a > b = 0
             | a == b = if isPrime a then a else 0 
             | isPrime b = primeSum a (b-1) + b
             | otherwise = primeSum a (b-1)

