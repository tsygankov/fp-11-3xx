-- Срок: 2016-04-02 (100%), 2016-04-07 (50%)

module HW4
       ( Show' (..)
       , A(..)
       , C(..)
       , Set(..)
       , symmetricDifference
       , fromBool
       , fromInt
       ) where

class Show' a where
  show' :: a -> String

data A = A Int
       | B
--show' (A 5)    == "A 5"
-- show' (A (-5)) == "A (-5)"
-- show' B        == "B"

showNum num | num >= 0 = show num
            | otherwise = "(" ++ show num ++ ")" 

instance Show' A where
      show' (A num) = "A " ++ showNum num
      show' B = "B"

data C = C Int Int
-- show' (C 1 2)       == "C 1 2"
-- show' (C (-1) (-2)) == "C (-1) (-2)"

instance Show' C where
      show' (C x y) = "C " ++ showNum x ++ " " ++ showNum y 

----------------

data Set a = Set (a -> Bool)

-- Симметрическая разность -
-- элементы обоих множеств, не входящие в объединение
-- {4,5,6} \/ {5,6,7,8} == {4,7,8}
symmetricDifference :: Set a -> Set a -> Set a
symmetricDifference (Set f) (Set g) =  Set $ \x -> (f x && not (g x)) || (not (f x) && g x)
-----------------

-- fromBool - переводит булевское значение в кодировку Чёрча
fromBool val | val = \t _ -> t
             | otherwise = \_ f -> f

-- fromInt - переводит число в кодировку Чёрча
fromInt val | val==0 = \_ z -> z
            | otherwise = \s z -> s ((fromInt (val - 1)) s z)

