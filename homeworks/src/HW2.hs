-- Тесты чуть позже

module HW2
       ( Contact (..)
       , isKnown
       , Term (..)
       , eval
       , simplify
       ) where

data Contact = On
             | Off
             | Unknown

isKnown :: Contact -> Bool
isKnown Unknown = False
isKnown _ = True
 
data Term = Mult Term Term      -- умножение
          | Add Term Term       -- сложение
          | Sub Term Term       -- вычитание
          | Const Int           -- константа


eval :: Term -> Int
eval (Add term1 term2)= eval term1 + eval term2
eval (Sub term1 term2)= eval term1 - eval term2
eval (Mult term1 term2)= eval term1 * eval term2
eval (Const num) = num
 

-- Раскрыть скобки
-- Mult (Add (Const 1) (Const 2)) (Const 3) ->
-- Add (Mult (Const 1) (Const 3)) (Mult (Const 2) (Const 3))
-- (1+2)*3 -> 1*3+2*3
simplify :: Term -> Term
simplify (Mult (Add x y) term2) = simplify (Add (Mult x term2) (Mult y term2))
simplify (Mult term1 (Add x y)) = simplify (Add (Mult term1 x) (Mult term1 y))
simplify (Mult (Sub x y) term2) = simplify (Sub (Mult x term2) (Mult y term2))
simplify (Mult term1 (Sub x y)) = simplify (Sub (Mult term1 x) (Mult term1 y))

simplify (Const x) = Const x
simplify (Add term1 term2) = Add (simplify term1) (simplify term2)
simplify (Sub term1 term2) = Sub (simplify term1) (simplify term2)
simplify (Mult term1 term2) = simplify2 (Mult (simplify term1) (simplify term2))

simplify2 :: Term -> Term
simplify2 (Mult (Add x y) term2) = simplify (Add (Mult x term2) (Mult y term2))
simplify2 (Mult term1 (Add x y)) = simplify (Add (Mult term1 x) (Mult term1 y))
simplify2 (Mult (Sub x y) term2) = simplify (Sub (Mult x term2) (Mult y term2))
simplify2 (Mult term1 (Sub x y)) = simplify (Sub (Mult term1 x) (Mult term1 y))
simplify2 (Mult term1 term2) = Mult term1 term2

